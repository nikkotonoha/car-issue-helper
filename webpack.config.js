let path = require('path');

module.exports = {

    entry: "./src/index.js",

    output:{
        path: path.resolve(__dirname, './public'),
        publicPath: '/public/',
        filename: "bundle.js"       
    },

    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },

            {
                test:/\.css$/,
                use:['style-loader','css-loader']
            },

        ]
    }
};